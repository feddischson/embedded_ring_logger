
Synopsis
=========
This is header-only small ring buffer logging library.
The goal is to have a small logging buffer 
 - with static memory,
 - which supports multiple tasks,
 - which supports multiple systems & RTOSs and
 - runs in embedded environments

The file `embedded_ring_logger.hpp` holds a base class, which needs to be derived
by a concret logging class.
The concret class needs to implement a few OS-specic methods and functionality like
 - a queue / event mechanism
 - mutex

See example.cpp how this can be done on a OS where std::thread and std::mutex is available.

Set the define `#define EMBEDDED_RING_LOGGER_NO_IOMANIP 1` to disable the inclusion and usage of setw/left/right (which uses dynamic memory allocation).
