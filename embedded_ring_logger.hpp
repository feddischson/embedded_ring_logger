// MIT License
//
// Copyright (c) 2019 Christian Haettich <feddischson@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#ifndef EMBEDDED_RING_LOGGER
#define EMBEDDED_RING_LOGGER

#include <array>

#ifndef EMBEDDED_RING_LOGGER_NO_IOMANIP
#define EMBEDDED_RING_LOGGER_NO_IOMANIP 0
#endif

#if EMBEDDED_RING_LOGGER_NO_IOMANIP == 0
#include <iomanip>
#endif
#include <sstream>
// If a specific assert function is used, provide it via
// EMBEDDED_RING_LOGGER_ASSERT
#ifndef EMBEDDED_RING_LOGGER_ASSERT
#include <cassert>
#define EMBEDDED_RING_LOGGER_ASSERT(_x_) assert(_x_);
#endif

namespace Embedded_Ring_Logger {

/// @brief Log level definition
enum class Level : int {
  None = 0,
  Critical = 1,
  Error = 2,
  Warning = 3,
  Info = 4,
  Debug = 5
};

static constexpr char level_names[6][12] = {"    [None] ", "[Critical] ",
                                            "   [Error] ", " [Warning] ",
                                            "    [Info] ", "   [Debug] "};

/// @brief Logging base class.
/// @details
///   Derived classes must implement OS-specific functionality:
///     - notify(), a notification mechnism that a new log-entry is available
///                 to unblock a 'consumer' task
///     - lock(), usually via mutex
///     - unlock(), usually via mutex
/// @tparam LEVEL The loglevel
/// @tparam N_MSG The maximum number of log-messages
/// @tparam MAX_MSG_LENGTH Maximum message size for each log-message
/// @tparam PRINT_LEVEL The log level is printed when set to true (default)
/// @tparam PRINTUNIT The unit is printed when set to true (default)
/// @tparam PRINT_LINE The line is printed when set to true (default)
/// @tparam IDX_T The type of the internal counters (default = uint8_t)
template <Level LEVEL, unsigned N_MSG, unsigned MAX_MSG_LENGTH,
          typename IDX_T = std::uint8_t, unsigned MAX_UNIT_W = 20,
          bool PRINT_TIME = true, bool PRINT_LEVEL = true,
          bool PRINT_UNIT = true, bool PRINT_LINE = true,
          bool PRINT_CHANNEL = true>
class Logging {
 public:
  /// @brief Ctor: Initializes all local members, nothing more.
  Logging() : cnt{0}, i_start{0}, i_stop{0}, messages{{'\0'}} {}

  /// @brief  Copy ctor: not implemented!
  Logging(const Logging &other) = delete;

  /// @brief  Assignment operator: not implemented!
  Logging &operator=(const Logging &rhs) = delete;

  /// @brief  Move constructor: not implemented!
  Logging(Logging &&rhs) = delete;

  /// @brief  Move assignment operator: not implemented!
  Logging &operator=(Logging &&rhs) = delete;

  /// @brief Default dtor
  virtual ~Logging() = default;

  /// @brief Shall notify the logging task, that a new entry is available.
  /// @param idx The index of the message.
  virtual void notify(IDX_T idx) = 0;

  /// @brief Shall realize a mutex-lock to protect cnt, i_start and i_stop.
  virtual void lock() = 0;

  /// @brief Shall realize a mutex-unlock to protect cnt, i_start and i_stop.
  virtual void unlock() = 0;

  /// @brief Initiates a logging step.
  /// @details Every log_start() must be followed by a log_done()
  /// @param time A time value
  /// @param line_no The line number of the log entry
  /// @param level The log level of this logging step
  /// @param unit The name of the file or the unit.
  ///             If __FILE__ is used, the string could get long in case of
  ///             absolute paths, so a shorter file or unit name can be used.
  /// @param channel_no A channel index. If < 0, the output is skipped.
  ///
  std::stringstream *log_start(unsigned time, unsigned line_no, Level level,
                               const std::string &unit = "",
                               int channel_no = -1) {
    EMBEDDED_RING_LOGGER_ASSERT(cnt != N_MSG);
    lock();
    IDX_T idx = i_start;

    // clear the buffer
    for (unsigned int i_char = 0; i_char < MAX_MSG_LENGTH; i_char++) {
      messages[idx][i_char] = '\0';
    }

    // assign buffer to stringstream
    streams[idx].rdbuf()->pubsetbuf(&messages[idx][0], MAX_MSG_LENGTH);

    // add unit and line_no to the entry
    if (PRINT_TIME) {
      streams[idx]
#if EMBEDDED_RING_LOGGER_NO_IOMANIP == 0
          << std::setw(12) << std::left
#endif
          << time << " | ";
    }
    if (PRINT_LEVEL) {
      streams[idx] << level_names[static_cast<unsigned>(level)];
    }

    if (PRINT_CHANNEL && channel_no >= 0) {
      streams[idx]
#if EMBEDDED_RING_LOGGER_NO_IOMANIP == 0
          << std::setw(4) << std::left
#endif
          << "[" << channel_no << "] ";
    }

    if (PRINT_UNIT) {
      streams[idx]
#if EMBEDDED_RING_LOGGER_NO_IOMANIP == 0
          << std::setw(MAX_UNIT_W) << std::right
#endif
          << unit;
    }

    if (PRINT_UNIT && PRINT_LINE) {
      streams[idx] << ":";
    }
    if (PRINT_LINE) {
      streams[idx]
#if EMBEDDED_RING_LOGGER_NO_IOMANIP == 0
          << std::setw(4) << std::left
#endif
          << line_no;
    }
    if (PRINT_UNIT || PRINT_LINE) {
      streams[idx] << " ";
    }

    (void)level;

    // return the stream to do the further processing
    return &streams[idx];
  }

  /// @brief Ends a logging step.
  /// @details It updates the counters calls notify that the 'consumer' task can
  ///          do the job.
  void log_done() {
    IDX_T idx = i_start;
    ++i_start;
    if (N_MSG == i_start) {
      i_start = 0;
    }
    ++cnt;
    unlock();
    notify(idx);
  }

  /// @brief Sets the precision in all streams entries.
  std::streamsize precision(std::streamsize prec) {
    std::streamsize ret;
    for (auto &s : streams) {
      ret = s.precision(prec);
    }
    return ret;
  }

  /// @brief The log level.
  static constexpr Level level = LEVEL;

 protected:
  /// @brief Writes a logging entry into os and updates the counters.
  void write(std::ostream &os, IDX_T no) {
    os << messages[no] << std::endl;
    write_done(no);
  }

  /// @brief can be used to access a raw message.
  /// @details This method can be used when write() is not suitable and the
  ///          message is processed somehow else
  ///          (like written to an UART on an embedded platform).
  char *message(IDX_T no) { return messages[no]; }

  /// @brief Updates the counters after 'consuming' the log entry 'no'.
  /// @details This method can be used when write() is not suitable and the
  ///          message is processed somehow else
  ///          (like written to an UART on an embedded platform).
  void write_done(IDX_T no) {
    lock();
    EMBEDDED_RING_LOGGER_ASSERT(no == i_stop);
    EMBEDDED_RING_LOGGER_ASSERT(cnt > 0);
    --cnt;
    ++i_stop;
    if (N_MSG == i_stop) {
      i_stop = 0;
    }
    unlock();
  }

 private:
  /// @brief Holds a counter which says, how much entries are in the buffer
  IDX_T cnt;

  /// @brief Index of the next free entry (start of the free part).
  IDX_T i_start;

  /// @brief Index of the first non-free entry.
  IDX_T i_stop;

  /// @brief Message-buffer, which is used as ring-buffer.
  char messages[N_MSG][MAX_MSG_LENGTH + 1];

  /// @brief Internal string-streams, used to have a stream operator syntax.
  std::stringstream streams[N_MSG];
};  // class Logging

}  // namespace Embedded_Ring_Logger

#endif  // EMBEDDED_RING_LOGGER

// vim: filetype=cpp et ts=2 sw=2 sts=2
