// MIT License
//
// Copyright (c) 2019 Christian Haettich <feddischson@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
#include <condition_variable>
#include <ctime>
#include <chrono>
#include <deque>
#include <fstream>
#include <iostream>
#include <mutex>
#include <thread>

#include "embedded_ring_logger.hpp"

using Embedded_Ring_Logger::Level;
using Embedded_Ring_Logger::Logging;
using std::cout;
using std::int32_t;
using std::ofstream;

/// @brief Example logging class
template <Level LEVEL, unsigned N_MSG, unsigned MAX_MSG_LENGTH,
          typename IDX_T = std::uint8_t>
class My_Logger : public Logging<LEVEL, N_MSG, MAX_MSG_LENGTH, IDX_T, 21, true,
                                 true, true> {
 public:
  My_Logger() : done(false) {}
  My_Logger(const My_Logger &other) = delete;
  My_Logger &operator=(const My_Logger &rhs) = delete;
  My_Logger(My_Logger &&rhs) = delete;
  My_Logger &operator=(My_Logger &&rhs) = delete;
  ~My_Logger() override { logging_thread.join(); };

  void notify(IDX_T no) override {
    std::unique_lock<std::mutex> mlock(log_mutex);
    log_queue.push_back(no);
    log_cond.notify_one();
  }

  void run() {
    while (true) {
      IDX_T no;
      {
        std::unique_lock<std::mutex> mlock(log_mutex);
        while (log_queue.empty() && !done) {
          log_cond.wait(mlock);
        }
        if (done && log_queue.empty()) {
          break;
        }
        no = log_queue.front();
        log_queue.pop_front();
      }
      this->write(std::cout, no);
    }
  }
  void lock() override { log_mutex.lock(); }
  void unlock() override { log_mutex.unlock(); }
  void start() { logging_thread = std::thread(&My_Logger::run, this); }
  void stop() { done = true; }

 private:
  bool done;
  std::thread logging_thread;
  std::mutex log_mutex;
  std::deque<IDX_T> log_queue;
  std::condition_variable log_cond;
};

My_Logger<Level::Debug, 5, 54> my_logger;

#define LOG(_level_, _unit_)                                     \
  if ((_level_) > my_logger.level)                               \
    ;                                                            \
  else                                                           \
    *my_logger.log_start(                                        \
        std::chrono::duration_cast<std::chrono::milliseconds>(   \
            std::chrono::system_clock::now().time_since_epoch()) \
            .count(),                                            \
        __LINE__, _level_, _unit_)

#define LOG_DONE my_logger.log_done()

int main(int, char *[]) {
  // start logger thread
  my_logger.start();

  LOG(Level::Info, "example.cpp") << "This is the " << 1 << "st"
                                  << " log entry"
                                  << " some more stuff",
      LOG_DONE;
  LOG(Level::Debug, "long_name_example.cpp") << "This is the " << 2 << "nd"
                                             << " log entry"
                                             << " some more stuff",
      LOG_DONE;
  LOG(Level::Info, "example.cpp") << "This is the " << 3 << "rd"
                                  << " log entry"
                                  << " some more stuff",
      LOG_DONE;

  // stop the logger thread
  my_logger.stop();
}

// vim: filetype=cpp et ts=2 sw=2 sts=2
